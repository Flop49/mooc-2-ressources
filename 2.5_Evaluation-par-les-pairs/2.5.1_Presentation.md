## Travail à faire

Nous arrivons à la fin de ce bloc 2, qui nous a permi de pratiquer la création de ressources pédagogiques. Cette dernière activité, évaluée par les pairs reprend l'ensemble de ce qui a été demandé dans les quatre premiers modules.

Pour réaliser ce travail, vous pouvez reprendre ce que vous avez déjà produit lors des activités précédentes (et que vous aurez probablement amélioré, corrigé au fil des échanges sur le forum) mais vous pouvez aussi créer des ressources nouvelles.

Vous devrez, sur un thème du programme NSI de votre choix :

1. Créer une activité élève et la fiche _prof_ associée et les mettre à disposition au téléchargement sur votre espace de ressources.
2. Créer une analyse d'intention de l'activité élève et la mettre à disposition au téléchargement sur votre espace de ressources.
3. Choisir et proposer une des activités suivantes :
    - une remédiation pour élèves en difficulté
    - une activité d'approfondissement
    - un mini projet
    On prendra soin de bien préciser  le contexte et la nature de votre activité
4. Si pour le point 3, vous avez choisi le mini projet, vous devrez proposer ici une grille d'évaluation adaptée ; sinon, proposer une évaluation de type devoir sur table.

Une fois toutes vos ressources créées et déposées sur votre espace, vous devrez consigner dans un unique fichier _markdown_ les liens d'accès à ces fichiers et le déposer sur la plate-forme FUN pour le processus d'évaluation par les pairs.

Il vous faudra ensuite corriger deux travaux suivant la grille d'évaluation suivante.
